﻿using System;
namespace BerlinClock.checkOnOff
{
    public class GetOnOffCalled{

        private int lamps, onSigns;

        public GetOnOffCalled(int lamps,int onSigns) {
            this.lamps = lamps;
            this.onSigns = onSigns;
        }

        AssignOnOffToSign assignOnOff = new AssignOnOffToSign(0, 0,"");
        public string CallOnOff(int lamps,int onSigns){
            return assignOnOff.ImplementOnOff(lamps, onSigns, "R");
        }
    }
}
