﻿using System;
namespace BerlinClock.checkOnOff
{
    public class AssignOnOffToSign{

        private int lamps;
        private int onSigns;
        private string onSign;
     
        public AssignOnOffToSign(int lamps,int onSigns,string onSign){
            this.lamps = lamps;
            this.onSigns = onSigns;
            this.onSign = onSign;
        }

        public string ImplementOnOff (int lamps, int onSigns, string outDisplayedSign) {

            for (int i = 0; i < onSigns;i++) {
                outDisplayedSign += onSign;
            }
        
            for (int i = 0; i < lamps - onSigns;i++){
                outDisplayedSign += "O";
            }
         
            return outDisplayedSign;
        }
    }
}
