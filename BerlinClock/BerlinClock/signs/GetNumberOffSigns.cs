﻿using System;
namespace BerlinClock.signs
{
    public class GetNumberOffSigns{
        int number;
        public GetNumberOffSigns(int number){
            this.number = number;
        }
       
        public int GetTopNumberOfOnSigns() {
            return (number - (number % 5))/ 5;
        }
    }
}
