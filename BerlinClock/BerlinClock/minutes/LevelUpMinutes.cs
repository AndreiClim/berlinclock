﻿using System;
using BerlinClock.checkOnOff;
using BerlinClock.signs;

namespace BerlinClock.minutes
{
    public class LevelUpMinutes{
        private int number;
        public LevelUpMinutes(int number){
            this.number = number;
        }

        AssignOnOffToSign assignOnOff = new AssignOnOffToSign(0, 0, "");
        GetNumberOffSigns getNumberOffSigns = new GetNumberOffSigns(0);

        public string GetLevelUpMinutes (int v) {

            return assignOnOff.ImplementOnOff(11, getNumberOffSigns.GetTopNumberOfOnSigns(), "Y").Replace("YYY", "YYR");
            throw new NotImplementedException();
        }
    }
}
