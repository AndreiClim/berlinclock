﻿using System;
using System.Linq;
using BerlinClock.hours;
using BerlinClock.minutes;
using BerlinClock.seconds;

namespace BerlinClock
{
    public class BerlinClock{
      
   
        public static string[] BerlinClockImplementation(string time){

            LevelDownHours levelDownHours = new LevelDownHours(0);
            LevelUpHours levelUpHours = new LevelUpHours(0);
            LevelDownMinutes levelDownMinutes = new LevelDownMinutes(0);
            LevelUpMinutes levelUpMinutes = new LevelUpMinutes(0);
            GetSeconds getSeconds = new GetSeconds(0);

            int[] sequence = time.Split(':').Select(int.Parse).ToArray();
            return new string[] {
                getSeconds.GetSymbolsSeconds(sequence[2]),
                levelUpHours.getLevelUpHours(sequence[0]),
                levelDownHours.GetBottomLevelHours(sequence[0]),
                levelUpMinutes.GetLevelUpMinutes(sequence[1]),
                levelDownMinutes.GetLevelDownMinutes(sequence[1])
            };
        }
    }
}

